<?php 

    $pageTitle = 'Se connecter';
    require_once('./templates/head.html');

    
    require_once('./templates/navbar.html');


    ?>
        <div id="addform-box" class="container-mw">
            <div id="add-title-box">
                <h1>Se connecter</h1>
                <div><img src="./public/img/logo.png" alt="logo mycave"></div>

            </div>

            <form id="addform" action="./src/controllers/login.php" method="POST" >
                <div class="addformgroup">
                    <label for="name">Nom du caviste</label>
                    <input type="text" id="name" name="name">
                </div>
                <div class="addformgroup">
                    <label for="password">Mot de passe</label>
                    <input type="password" id="password" name="password">
                </div>
               
                <input type="submit" value="Se Connecter">
            </form>
            
        </div>
    <?php

    

    require_once('./templates/foot.html');

    ?>