<?php 
    $pageTitle = 'Accueil' ;
    require_once('./templates/head.html');
    
    require_once('./templates/navbar.html');
    require_once('./templates/topbox.html');

    require_once('./src/models/wines.php');

    require_once('./src/controllers/wines_index_list.php');
    
    require_once('./templates/foot.html');

    ?>