<?php 
if ( isset($_GET['id']) && !empty($_GET['id'])){
    $id = $_GET['id'];
}
else{
    header('Location', './index.php');
    exit;
}

    require_once('./src/models/wines.php'); 
    $wine = getDetailWine($id) ;
    $pageTitle = 'Détail - ' . htmlspecialchars(ucwords($wine['domain'])) ;
    require_once('./templates/head.html');
    
    require_once('./templates/navbar.html');

    ?>
        <div id="details-box" class="container">
            <h1 id="details-title">
                <?= htmlspecialchars(ucwords($wine['domain'])) ?>
            </h1>

            <div class="container" id="details-description">
                <p>
                    <?= htmlspecialchars(ucfirst($wine['description'])) ?>
                </p>
            </div>

            <div id="details-infos" class="line">
                <div class="box-12 box-md-6 details-info-part">               
                    <p> <span class="details-info-span">Pays :</span> <?= htmlspecialchars(ucwords($wine['country']))?>  </p>
                    <p> <span class="details-info-span">Région :</span> <?= htmlspecialchars(ucwords($wine['region']))?>  </p>
                </div>
                <div class="box-12 box-md-6 details-info-part">
                    <p> <span class="details-info-span">Cépage :</span> <?= htmlspecialchars(ucwords($wine['grape'])) ?></p>       
                    <p> <span class="details-info-span">Année :</span> <?= htmlspecialchars($wine['year']) ?></p>
                </div>
            </div>

            <div id="d-pict-box">
                    <img id="d-pict" src="./public/img/<?= $wine['picture'] ?>" alt="bottle image">
            </div>
            <?php
            if (isset($_SESSION['user']) && !empty($_SESSION['user']) ) {          
                $adminBtn = '<div class="line admin-btn" >
                                 <a class="admin-del box-12 box-sm-6" href="./delete_wine.php?id=' . $wine['id'] .'" >Supprimer</a>
                                 <a class="admin-upd box-12 box-sm-6" href="./update_wine.php?id=' . $wine['id'] .'" >Modifier</a>
                             </div>' ;      
             }
             else{
                 $adminBtn = '';
             }
             echo $adminBtn;
             ?>
        </div>
    <?php

    





    require_once('./templates/foot.html');

    ?>