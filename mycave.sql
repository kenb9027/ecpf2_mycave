-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mar. 04 mai 2021 à 08:43
-- Version du serveur :  8.0.18
-- Version de PHP :  7.4.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `mycave`
--

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `password` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `created_on`) VALUES
(1, 'Edgard', 'ed@mycave.fr', '1234', '2021-04-15 16:15:31'),
(2, 'Polo', 'polo@mycave.com', '1234', '2021-04-16 10:19:12');

-- --------------------------------------------------------

--
-- Structure de la table `wines`
--

DROP TABLE IF EXISTS `wines`;
CREATE TABLE IF NOT EXISTS `wines` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `domain` varchar(150) NOT NULL,
  `grape` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `region` varchar(150) NOT NULL,
  `country` varchar(150) NOT NULL,
  `year` int(11) NOT NULL,
  `description` text NOT NULL,
  `picture` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'generic.jpg',
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `wines`
--

INSERT INTO `wines` (`id`, `domain`, `grape`, `region`, `country`, `year`, `description`, `picture`, `created_on`) VALUES
(1, 'château de saint cosme', 'grenache / syrah', 'Southern Rhone / Gigondas', 'france', 2009, 'The aromas of fruit and spice give one a hint of the light drinkability of this lovely wine, which makes an excellent complement to fish dishes.\r\n', 'saint_cosme.jpg', '2021-04-15 08:55:52'),
(3, 'lan rioja crianza', 'tempranillo', 'rioja', 'spain', 2006, 'A resurgence of interest in boutique vineyards has opened the door for this excellent foray into the dessert wine market. Light and bouncy, with a hint of black truffle, this wine will not fail to tickle the taste buds.\r\n', 'lan_rioja.jpg', '2021-04-15 08:59:23'),
(4, 'margerum sybarite', 'sauvignon blanc', 'california central coast', 'usa', 2010, 'The cache of a fine Cabernet in ones wine cellar can now be replaced with a childishly playful wine bubbling over with tempting tastes of\r\nblack cherry and licorice. This is a taste sure to transport you back in time.\r\n', 'margerum.jpg', '2021-04-15 08:59:23'),
(5, 'owen roe \"ex umbris\"', 'syrah', 'washington', 'usa', 2009, 'A one-two punch of black pepper and jalapeno will send your senses reeling, as the orange essence snaps you back to reality. Don\'t miss\r\nthis award-winning taste sensation.\r\n', 'ex_umbris.jpg', '2021-04-15 09:01:12'),
(6, 'rex hill', 'pinot noir', 'oregon', 'usa', 2009, 'One cannot doubt that this will be the wine served at the Hollywood award shows, because it has undeniable star power. Be the first to catch\r\nthe debut that everyone will be talking about tomorrow.\r\n', 'rex_hill.jpg', '2021-04-15 09:01:12'),
(7, 'viticcio classico riserva', 'sangriovese merlot', 'tuscany', 'italy', 2007, 'Though soft and rounded in texture, the body of this wine is full and rich and oh-so-appealing. This delivery is even more impressive when one takes note of the tender tannins that leave the taste buds wholly satisfied.\r\n', 'viticcio.jpg', '2021-04-15 09:04:02'),
(8, 'château le doyenne', 'merlot', 'bordeaux', 'france', 2005, 'Though dense and chewy, this wine does not overpower with its finely balanced depth and structure. It is a truly luxurious experience for the\r\nsenses.\r\n', 'le_doyenne.jpg', '2021-04-15 09:04:02'),
(9, 'domaine du bouscat', 'merlot', 'bordeaux', 'france', 2009, 'The light golden color of this wine belies the bright flavor it holds. A true summer wine, it begs for a picnic lunch in a sun-soaked vineyard.\r\n', 'bouscat.jpg', '2021-04-15 09:06:13'),
(10, 'block nine', 'pinot noir', 'california', 'usa', 2009, 'With hints of ginger and spice, this wine makes an excellent complement to light appetizer and dessert fare for a holiday gathering.\r\n', 'block_nine.jpg', '2021-04-15 09:06:13'),
(11, 'domaine serene', 'pinot noir', 'california', 'usa', 2009, 'Though subtle in its complexities, this wine is sure to please a wide range of enthusiasts. Notes of pomegranate will delight as the nutty finish completes the picture of a fine sipping experience.\r\n', 'domaine_serene.jpg', '2021-04-15 09:07:53'),
(12, 'bodega lurton', 'pinot gris', 'mendoza', 'argentina', 2011, 'Though subtle in its complexities, this wine is sure to please a wide range of enthusiasts. Notes of pomegranate will delight as the nutty finish completes the picture of a fine sipping experience.', 'bodega_lurton.jpg', '2021-04-15 09:07:53'),
(13, 'les morizottes', 'chardonnay', 'burgundy', 'france', 2009, 'Breaking the mold of the classics, this offering will surprise and\r\nundoubtedly get tongues wagging with the hints of coffee and tobacco in\r\nperfect alignment with more traditional notes. Sure to please the late-night crowd with the slight jolt of adrenaline it brings.', 'morizottes.jpg', '2021-04-15 09:09:19'),
(26, 'Moda', 'Merlot', 'Anjou', 'France', 2013, 'Le Moda a la mode !', 'moda.jpg', '2021-04-16 10:42:20'),
(28, 'Domaine de la Grappe folle', 'Cabernet', 'Burgundy', 'France', 2016, 'Lorem Tagada pouet pouet !', 'petitoursponey1.jpg', '2021-04-20 09:20:45'),
(29, 'Jacob\'s Creek', 'Syrah / Cabernet', 'Perth', 'Australia', 2018, 'lorem test pouet pouet', 'arshla-jindal-JTPrf0at-6I-unsplash(1).jpg', '2021-04-20 10:56:16'),
(30, 'Domaine Test 01', 'Merlot', 'Languedoc', 'France', 2010, 'dz dzd zdzd zd zd zd z', 'mouette.jpg', '2021-05-04 09:03:17'),
(31, 'Test 02', 'Merlot', 'Anjou', 'France', 2003, 'DJAZ DJZIDJ ZAID ZAIOD HOIZDH IOHAZIO HDZIAOH DIZOAHD IAZOH D', 'petitoursponey.jpg', '2021-05-04 09:15:53'),
(32, 'test 3', 'Merlot', 'Anjou', 'France', 2010, 'zdz zd zfe lm mz zj zmljz mzjd ozjdzm ljz ', 'mouette.jpg', '2021-05-04 09:16:40'),
(33, 'test 4', 'Merlot', 'Anjou', 'France', 2010, 'jdkzh dzh ', 'mouette2.jpg', '2021-05-04 09:19:31'),
(34, 'chateau test 55', 'Merlot', 'Anjou', 'France', 2008, 'edzdz dz dz ', 'mouette3.jpg', '2021-05-04 09:20:14'),
(35, 'test 6', 'Merlot', 'Anjou', 'France', 2007, 'lmkmkmkmkmkmk', 'mouette4.jpg', '2021-05-04 09:22:21'),
(36, 'Karl ; ?> <?php echo \"slurp\"; die; ?>', 'Sauvignon', 'Anjou', 'France', 2015, 'Karl love\'\' it !', 'karl-marx.jpg', '2021-05-04 09:50:22');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
