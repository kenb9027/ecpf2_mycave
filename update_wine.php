<?php 
    if ( isset($_GET['id']) && !empty($_GET['id'])){
        $id = $_GET['id'] ;
    }
    else {
        header('Location: ../../error.php');
        exit;
    }

    require_once('./src/models/wines.php');
    $wine = getDetailWine($id);
    $pageTitle = 'Modifier '. ucwords($wine['domain']) ;
    require_once('./templates/head.html');


    
    require_once('./templates/navbar.html');


    ?>
        <div id="addform-box" class="container-mw">
            <div id="add-title-box">
                <h1>Modifier </h1>
                <h2><?= htmlspecialchars(ucwords($wine['domain'])) ?></h2>

            </div>
            
            <div id="upd-last-pict"><img src="./public/img/<?= htmlspecialchars($wine['picture']) ?>" alt="image de la bouteille"></div>


            <form id="addform" action="./src/controllers/updatewine.php" method="POST" enctype="multipart/form-data" >
            <input type="hidden" name="id" value="<?= $id ?>">
                <div class="addformgroup">
                    <label for="domain">Domaine</label>
                    <input type="text" id="domain" name="domain" value="<?= htmlspecialchars($wine['domain']) ?>">
                </div>
                <div class="addformgroup">
                    <label for="grape">Cépage</label>
                    <input type="text" id="grape" name="grape" value="<?= htmlspecialchars($wine['grape']) ?>">
                </div>
                <div class="addformgroup">
                    <label for="region">Région</label>
                    <input type="text" id="region" name="region" value="<?= htmlspecialchars($wine['region']) ?>">
                </div>
                <div class="addformgroup">
                    <label for="country">Pays</label>
                    <input type="text" id="country" name="country" value="<?= htmlspecialchars($wine['country']) ?>">
                </div>
                <div class="addformgroup">
                    <label for="year">Année</label>
                    <input type="number" id="year" name="year" min="1900" max="2030"  value="<?= htmlspecialchars($wine['year']) ?>" >
                </div>
                
                <label for="description">Description</label>
                <textarea type="text" id="description" name="description" cols="30" rows="10" ><?= htmlspecialchars($wine['description']) ?></textarea>
                
                <div class="addformgroup">
                    <label class="add-lab-img" for="picture">Image</label>
                    <input type="file" id="picture" name="picture">
                    <input type="hidden" name="expicture" value="<?= htmlspecialchars($wine['picture']) ?>">
                </div>
        
                <input type="submit" value="Modifier cette bouteille">
            </form>
            
        </div>
    <?php

    

    require_once('./templates/foot.html');

    ?>