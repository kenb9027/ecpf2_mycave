<?php
$wineInfo = [] ;

if ( isset($_POST['domain']) ){
    $wineInfo['domain'] = htmlspecialchars($_POST['domain']);
}
else{
    $wineInfo['domain'] = ' ';

}
if ( isset($_POST['grape']) ){
    $wineInfo['grape'] = htmlspecialchars($_POST['grape']);
}
else{
    $wineInfo['grape'] = ' ';

}
if ( isset($_POST['region']) ){
    $wineInfo['region'] = htmlspecialchars($_POST['region']);
}
else{
    $wineInfo['region'] = ' ';
}
if ( isset($_POST['country'])  ){
    $wineInfo['country'] = htmlspecialchars($_POST['country']);
}
else{
    $wineInfo['country'] = ' ';
}
if ( isset($_POST['year'])  ){
    $wineInfo['year'] = htmlspecialchars($_POST['year']);
}
else{
    $wineInfo['year'] = ' ';
}


require_once('./src/models/wines.php');

$wines = filterWines($wineInfo);
