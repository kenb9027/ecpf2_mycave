<div id="index-wines-grid" class="container" >
    <h1 id="index-title">Liste des vins disponibles</h1>
    <div class="line">
        <?php  
        $wines = getIndexWines(); 
        foreach ($wines as $wine) {
            if (isset($_SESSION['user']) && !empty($_SESSION['user']) ) {          
                $adminBtn = '<div class="line admin-btn" >
                                 <a class="admin-del box-12 box-sm-6" href="./delete_wine.php?id=' . $wine['id'] .'" >Supprimer</a>
                                 <a class="admin-upd box-12 box-sm-6" href="./update_wine.php?id=' . $wine['id'] .'" >Modifier</a>
                             </div>' ;      
             }
             else{
                 $adminBtn = '<div class="line admin-btn" >
                                <a class="admin-del box-6 " href="../../details.php?id=' . $wine['id'] .'" >Détails</a>
                            </div>';
             }
                echo '<div class=" index-bottle box-12 box-md-6" >
                        <h2 class="bottle-name" ><a href="./details.php?id='.$wine['id'].'" >' . htmlspecialchars(ucwords($wine['domain'])) .'</a></h2>
                        <div class="line infos-box">
                            <div class="box-12 box-sm-6 bottle-infos">
                                <h3>'. htmlspecialchars(ucfirst($wine['country'])) .'</h3>
                                <p> <span class="index-bold">Région</span> : ' . htmlspecialchars(ucwords($wine['region'])) .'  </p>
                                <p> <span class="index-bold">Cépage</span> : '. htmlspecialchars(ucwords($wine['grape'])) .'</p>       
                                <p> <span class="index-bold">Année</span> : '. htmlspecialchars($wine['year']) .'</p>  
                                
                            </div>
                            <div class="box-12 box-sm-6 img-part">
                                <img class="bottle-img" src="./public/img/'. htmlspecialchars($wine['picture']) .'" alt="a wine bottle" /> 
                            </div>
                        </div>
                        '.
                        $adminBtn
                        .'
                    </div>'; 
        }
        ?>
    </div>
</div>