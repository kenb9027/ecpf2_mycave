<?php
$user = [] ;

if ( isset($_POST['name']) && !empty($_POST['name']) ){
    $user['name'] = $_POST['name'];
}
else{
    header('Location: ../../index.php');
    exit;
}
if ( isset($_POST['password']) && !empty($_POST['password']) ){
    $user['password'] = $_POST['password'];
}
else{
    header('Location: ../../index.php');
    exit;
}

require_once('../models/users.php');

if (login($user)){

    session_start();
    $idLogged = $user['id'];
    $nameLogged = $user['name'];

    $_SESSION['user'] = ['id' => $idLogged , 'name' => $nameLogged] ;

    header('Location: ../../index.php');
    exit;
}
else {
    header('Location: ../../error.php');
    exit;
}