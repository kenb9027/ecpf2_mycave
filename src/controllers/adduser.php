<?php
$newUser = [] ;
if ( isset($_POST['name']) && !empty($_POST['name']) ){
    $newUser['name'] =  htmlentities($_POST['name'],ENT_QUOTES);
}
else{
    header('Location: ../../index.php');
    exit;
}
if ( isset($_POST['email']) && !empty($_POST['email']) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) ){
    $newUser['email'] =  htmlentities($_POST['email'],ENT_QUOTES);
}
else{
    header('Location: ../../index.php');
    exit;
}
if ( isset($_POST['password']) && !empty($_POST['password']) ){
    $newUser['password'] = $_POST['password'];
}
else{
    header('Location: ../../index.php');
    exit;
}

require_once('../models/users.php');

// var_dump(addUser($newUser));die;
if (addUser($newUser)){

    header('Location: ../../index.php');
    exit;
}
else {
    header('Location: ../../error.php');
    exit;
}