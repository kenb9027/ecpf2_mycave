<?php
$wineInfo = [] ;

if ( isset($_POST['domain']) && !empty($_POST['domain']) ){
    $wineInfo['domain'] = htmlspecialchars($_POST['domain']);
}
else{
    header('Location: ../../index.php');
    exit;
}
if ( isset($_POST['grape']) && !empty($_POST['grape']) ){
    $wineInfo['grape'] = htmlspecialchars($_POST['grape']);
}
else{
    header('Location: ../../index.php');
    exit;
}
if ( isset($_POST['region']) && !empty($_POST['region']) ){
    $wineInfo['region'] = htmlspecialchars($_POST['region']);
}
else{
    header('Location: ../../index.php');
    exit;
}
if ( isset($_POST['country']) && !empty($_POST['country']) ){
    $wineInfo['country'] = htmlspecialchars($_POST['country']);
}
else{
    header('Location: ../../index.php');
    exit;
}
if ( isset($_POST['year']) && !empty($_POST['year']) ){
    $wineInfo['year'] = htmlspecialchars($_POST['year']);
}
else{
    header('Location: ../../index.php');
    exit;
}
if ( isset($_POST['description']) && !empty($_POST['description']) ){
    $wineInfo['description'] = htmlspecialchars($_POST['description']);
}
else{
    header('Location: ../../index.php');
    exit;
}
if($picture['size'] > 5000000) {
    header('Location: ../../index.php');
    exit;
}
require_once('../models/wines.php');

if($_FILES['picture']['name'] == ''){
    $picture = 'generic.jpg';
    $wineInfo['picture'] = $picture ;
}else{
    $picture = $_FILES['picture']['name'];

    $name = pathinfo($_FILES['picture']['name'], PATHINFO_FILENAME);
    $extension = pathinfo($_FILES['picture']['name'], PATHINFO_EXTENSION);
    $dirname = "../../public/img/";
    $increment = ''; //start with no suffix
    
    
    while(file_exists($dirname . $name . $increment . '.' . $extension)) {
        $increment++;
    }
    $basename = $name . $increment . '.' . $extension;
    $resultFilePath = $dirname . $name . $increment . '.' . $extension;
    $wineInfo['picture'] = $basename ;
};
if (addWine($wineInfo)){
    if($_FILES['picture']['name'] != ''){
        move_uploaded_file($_FILES['picture']['tmp_name'], '../../public/img/'.$basename);
    }
    header('Location: ../../index.php');
    exit;
}
else {
    header('Location: ../../error.php');
    exit;
}