<?php
$wineInfo = [] ;
if ( isset($_POST['id']) && !empty($_POST['id']) ){
    $wineInfo['id'] = htmlspecialchars($_POST['id']);
}
else{
    header('Location: ../../index.php');
    exit;
}
if ( isset($_POST['domain']) && !empty($_POST['domain']) ){
    $wineInfo['domain'] = htmlspecialchars($_POST['domain']);
}
else{
    header('Location: ../../index.php');
    exit;
}
if ( isset($_POST['grape']) && !empty($_POST['grape']) ){
    $wineInfo['grape'] = htmlspecialchars($_POST['grape']);
}
else{
    header('Location: ../../index.php');
    exit;
}
if ( isset($_POST['region']) && !empty($_POST['region']) ){
    $wineInfo['region'] = htmlspecialchars($_POST['region']);
}
else{
    header('Location: ../../index.php');
    exit;
}
if ( isset($_POST['country']) && !empty($_POST['country']) ){
    $wineInfo['country'] = htmlspecialchars($_POST['country']);
}
else{
    header('Location: ../../index.php');
    exit;
}
if ( isset($_POST['year']) && !empty($_POST['year']) ){
    $wineInfo['year'] = htmlspecialchars(($_POST['year']));
}
else{
    header('Location: ../../index.php');
    exit;
}
if ( isset($_POST['description']) && !empty($_POST['description']) ){
    $wineInfo['description'] = htmlspecialchars($_POST['description']);
}
else{
    header('Location: ../../index.php');
    exit;
}
if($picture['size'] > 5000000) {
    header('Location: ../../index.php');
    exit;
}
require_once('../models/wines.php');
if($_FILES['picture']['name'] == ''){
    $picture = $_POST['expicture'];
    $wineInfo['picture'] = $picture;
}else{
    // on vérifie la taille exacte du fichier
    
    $picture = $_FILES['picture']['name'];
    $name = pathinfo($picture, PATHINFO_FILENAME);
    $extension = pathinfo($picture, PATHINFO_EXTENSION);
    $dirname = "../../public/img/";
    $increment = ''; //start with no suffix


    while(file_exists($dirname . $name . $increment . '.' . $extension)) {
        $increment++;
    }
    $basename = $name . $increment . '.' . $extension;
    $resultFilePath = $dirname . $name . $increment . '.' . $extension;

    $wineInfo['picture'] = $basename ;
};

if (updateWine($wineInfo)){
    if($_FILES['picture']['name'] != ''){
        move_uploaded_file($_FILES['picture']['tmp_name'], '../../public/img/'.$wineInfo['picture']);
    }
    $id = $wineInfo['id'];
    header('Location: ../../details.php?id='.$id);
    exit;
}
else {
    header('Location: ../../error.php');
    exit;
}