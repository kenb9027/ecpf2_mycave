<?php
if ( isset($_GET['id']) && !empty($_GET['id'])){
    $id = $_GET['id'] ;
}
else {
    header('Location: ../../error.php');
    exit;
}
require_once('../models/wines.php');

if (deleteWine($id)){

    header('Location: ../../index.php');
    exit;
}
else {
    header('Location: ../../error.php');
    exit;
}