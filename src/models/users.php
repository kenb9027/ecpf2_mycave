<?php
function connectBdd()
{
        try {        // On se connecte à MySQL
                $pdo = new PDO('mysql:host=localhost;dbname=mycave;charset=utf8', 'root', '');
                return $pdo;
        } catch (Exception $e) {        // En cas d'erreur, on affiche un message et on arrête tout
                die('Erreur : ' . $e->getMessage());
        }
}

function addUser(array $userInfo)
{
        $sql = "INSERT INTO users (name, email, password) VALUES (:name , :email, :password)";
        try {
                $pdo = connectBdd();
                $stmt = $pdo->prepare($sql);
                $stmt->bindParam(':name', $userInfo['name']);
                $stmt->bindParam(':email', $userInfo['email']);
                $stmt->bindParam(':password', password_hash($userInfo['password'], PASSWORD_DEFAULT));


                return $stmt->execute();
        } catch (\Throwable $th) {
                echo 'mince...';
                header('Location: ./index.php ');
        }
}

function login($userInfo)
{
        $sql = "SELECT `id`, `name`, `email`, `password`, `created_on`, `updated_on` FROM `users` WHERE name = :name ";
        try {
                $pdo = connectBdd();
                $stmt = $pdo->prepare($sql);
                $stmt->bindParam(':name', $userInfo['name']);
                $stmt->execute();
                $res = $stmt->fetch(PDO::FETCH_ASSOC);
                
                if (!password_verify($userInfo['password'], $res['password'])) {
                        header('Location: ./index.php ');
                }
                return $res;
        } catch (\Throwable $th) {
                echo 'mince...';
                header('Location: ./index.php ');
        }
}
