<?php 
function connectBdd() {
        try
        {	// On se connecte à MySQL
                $pdo = new PDO('mysql:host=localhost;dbname=mycave;charset=utf8', 'root', '');
                return $pdo ;
        }
        catch(Exception $e)
        {	// En cas d'erreur, on affiche un message et on arrête tout
                die('Erreur : '.$e->getMessage());
        }
}

function getIndexWines() {

        $sql = "SELECT id, domain, grape, region, country, year, description, picture, created_on, updated_on FROM wines ORDER BY created_on DESC";

        try{
                $pdo = connectBdd();
                $stmt = $pdo->prepare($sql);
                $stmt->execute();

                $wines = $stmt->fetchAll(PDO::FETCH_ASSOC) ;
                return $wines ;


        }
        catch(\Throwable $th){
                echo 'mince...' ; 
                header('Location: ../../index.php ');
                }
}
function getDetailWine($id) {

        $sql = "SELECT id, domain, grape, region, country, year, description, picture, created_on, updated_on FROM wines WHERE id= :id ";

        try{
                $pdo = connectBdd();
                $stmt = $pdo->prepare($sql);
                $stmt->bindParam(':id' , $id);
                $stmt->execute();

                $wine = $stmt->fetch(PDO::FETCH_ASSOC) ;
                return $wine ;


        }
        catch(\Throwable $th){
                echo 'mince...' ; 
                header('Location: ../../index.php ');
                }
}

function addWine(array $wineInfo){
        $sql = "INSERT INTO wines ( domain, grape, region, country, year, description, picture) 
                VALUES ( :domain , :grape , :region , :country , :year , :description , :picture )";

        try{
                $pdo = connectBdd();
                $stmt = $pdo->prepare($sql);
                $stmt->bindParam(':domain' , $wineInfo['domain']);
                $stmt->bindParam(':grape' , $wineInfo['grape']);
                $stmt->bindParam(':region' , $wineInfo['region']);
                $stmt->bindParam(':country' , $wineInfo['country']);
                $stmt->bindParam(':year' , $wineInfo['year']);
                $stmt->bindParam(':description' , $wineInfo['description']);
                $stmt->bindParam(':picture' , $wineInfo['picture']);

                return $stmt->execute();
        }
        catch(\Throwable $th){
                echo 'mince...' ; 
                header('Location: ../../index.php ');
                }
}
function deleteWine($id){
        $sql= "DELETE FROM wines WHERE id= :id " ;
        try{
                $pdo = connectBdd();
                $stmt = $pdo->prepare($sql);
                $stmt->bindParam(':id' , $id);

                return $stmt->execute();
        }
        catch(\Throwable $th){
                echo 'mince...' ; 
                header('Location: ../../index.php ');
                }
}
function updateWine($updateInfo){
        $sql = "UPDATE `wines` SET `domain`= :domain ,`grape`= :grape ,`region`= :region ,`country`= :country,`year`= :year ,`description`= :description ,`picture`= :picture WHERE id= :id";

        try{
                $pdo = connectBdd();
                $stmt = $pdo->prepare($sql);
                $stmt->bindParam(':id' , $updateInfo['id']);
                $stmt->bindParam(':domain' , $updateInfo['domain']);
                $stmt->bindParam(':grape' , $updateInfo['grape']);
                $stmt->bindParam(':region' , $updateInfo['region']);
                $stmt->bindParam(':country' , $updateInfo['country']);
                $stmt->bindParam(':year' , $updateInfo['year']);
                $stmt->bindParam(':description' , $updateInfo['description']);
                $stmt->bindParam(':picture' , $updateInfo['picture']);
                
                return $stmt->execute();

        }
        catch(\Throwable $th){
                echo 'mince...' ; 
                header('Location: ../../index.php ');
                }
}

function filterWines($filterInfos){
        $sql = "SELECT id, domain, grape, region, country, year, picture, created_on
                FROM wines 
                WHERE domain LIKE :domain
                AND grape LIKE :grape
                AND region LIKE :region
                AND country LIKE :country
                AND year LIKE :year
                ORDER BY created_on DESC";

        try{
                $pdo = connectBdd();
                $stmt = $pdo->prepare($sql);
                $stmt->bindValue(':domain' , "%{$filterInfos['domain']}%");
                $stmt->bindValue(':grape' , "%{$filterInfos['grape']}%");
                $stmt->bindValue(':region' , "%{$filterInfos['region']}%");
                $stmt->bindValue(':country' , "%{$filterInfos['country']}%");
                $stmt->bindValue(':year' , "%{$filterInfos['year']}%");
                $stmt->execute();

                $wines = $stmt->fetchAll(PDO::FETCH_ASSOC) ;
                

                return $wines ;

        }
        catch(\Throwable $th){
                echo 'mince...' ; 
                header('Location: ../../index.php ');
                }

}
