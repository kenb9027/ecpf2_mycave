<?php 

    $pageTitle = 'Ajouter une bouteille';
    require_once('./templates/head.html');

    
    require_once('./templates/navbar.html');


    ?>
        <div id="addform-box" class="container-mw">
            <div id="add-title-box">
                <h1>Ajouter une bouteille à </h1>
                <div><img src="./public/img/logo.png" alt="logo mycave"></div>

            </div>

            <form id="addform" action="./src/controllers/addwine.php" method="POST" enctype="multipart/form-data" >
                <div class="addformgroup">
                    <label for="domain">Domaine</label>
                    <input type="text" id="domain" name="domain">
                </div>
                <div class="addformgroup">
                    <label for="grape">Cépage</label>
                    <input type="text" id="grape" name="grape">
                </div>
                <div class="addformgroup">
                    <label for="region">Région</label>
                    <input type="text" id="region" name="region">
                </div>
                <div class="addformgroup">
                    <label for="country">Pays</label>
                    <input type="text" id="country" name="country">
                </div>
                <div class="addformgroup">
                    <label for="year">Année</label>
                    <input type="number" id="year" name="year" min="1900" max="2030" >
                </div>
                
                <label for="description">Description</label>
                <textarea type="text" id="description" name="description" cols="30" rows="10" ></textarea>
                
                <div class="addformgroup">
                    <label class="add-lab-img" for="picture">Image</label>
                    <input type="file" id="picture" name="picture">
                </div>
        
                <input type="submit" value="Ajouter cette bouteille">
            </form>
            
        </div>
    <?php

    

    require_once('./templates/foot.html');

    ?>