<?php 
    if ( isset($_GET['id']) && !empty($_GET['id'])){
        $id = $_GET['id'] ;
    }
    else {
        header('Location: ../../error.php');
        exit;
    }

    require_once('./src/models/wines.php');

    $wine = getDetailWine(($id));

    $pageTitle = 'Suppression de '. $wine['domain'] ;
    require_once('./templates/head.html');

    
    require_once('./templates/navbar.html');

    require_once('./templates/topbox.html');

    ?>
    <div id="del-box" class="container">
    
    <h2 id="del-title">Souhaitez vous vraiment supprimer <?= $wine['domain'] ?> ?</h2>
    <p id="del-subtitle">Attention, cette action est irréversible</p>

    <a class="link del-confirm" href="./src/controllers/deletewine.php?id=<?= $wine['id'] ?>">Supprimer cette bouteille</a>


    <a class="link del-back" href="./index.php">Retour à la page d'accueil</a>

   
    </div>

    <?php





    require_once('./templates/foot.html');

    ?>