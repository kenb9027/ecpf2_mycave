<?php 
    $pageTitle = 'Rechercher une bouteille';
    require_once('./templates/head.html');   
    require_once('./templates/navbar.html');
    // require_once('./templates/topbox.html');
    ?>    
    
    <div class="container">
    <h1 id="filter-title" >Rechercher une bouteille</h1>
     <form id="filter-form" action="./filter_response.php" method="POST"> 
        <div class="form-group">
            <label for="domain">Domaine</label>
            <input type="text" name="domain" id="domain">
        </div>
        <div class="form-group">
            <label for="country">Pays</label>
            <input type="text" name="country" id="country">
        </div>
        <div class="form-group">
            <label for="region">Région</label>
            <input type="text" name="region" id="region">
        </div>
        <div class="form-group">
            <label for="grape">Cépages</label>
            <input type="text" name="grape" id="grape">
        </div>
        <div class="form-group">
            <label for="year">Année</label>
            <input type="number" name="year" id="year" >
        </div>
        <div class="form-group">
            <input type="submit" value="Rechercher">
        </div>
    </form>  
        

    </div>
   

    <?php

    

    require_once('./templates/foot.html');

    ?>