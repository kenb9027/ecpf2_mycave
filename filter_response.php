<?php 
    $pageTitle = 'Resultats de la recherche';
    require_once('./templates/head.html');   
    require_once('./templates/navbar.html');
    require_once('./src/controllers/filter.php');
    ?>    
    
    <div class="container">
    <h1 id="filter-title" >Resultats de la recherche : </h1>

    <div class="line search-infos">
        <div class="box-12 box-sm-6 box-md-4">
            Domaine :  <span><?= htmlspecialchars($wineInfo['domain']) ?></span>
        </div>
        <div class="box-12 box-sm-6 box-md-4">
            Cépage :  <span><?= htmlspecialchars($wineInfo['grape']) ?></span>
        </div>
        <div class="box-12 box-sm-6 box-md-4">
            Pays :  <span><?= htmlspecialchars($wineInfo['country']) ?></span>
        </div>
        <div class="box-12 box-sm-6 box-md-4">
            Région :  <span><?= htmlspecialchars($wineInfo['region']) ?></span>
        </div>
        <div class="box-12 box-sm-6 box-md-4">
            Année :  <span><?= htmlspecialchars($wineInfo['year']) ?></span>
        </div>
    </div>

    <div class="line">
        <?php  
        if (!empty($wines)){
            foreach ($wines as $wine) {
            if (isset($_SESSION['user']) && !empty($_SESSION['user']) ) {          
                $adminBtn = '<div class="line admin-btn" >
                                 <a class="admin-del box-12 box-sm-6" href="./delete_wine.php?id=' . $wine['id'] .'" >Supprimer</a>
                                 <a class="admin-upd box-12 box-sm-6" href="./update_wine.php?id=' . $wine['id'] .'" >Modifier</a>
                             </div>' ;      
             }
             else{
                 $adminBtn = '<div class="line admin-btn" >
                                <a class="admin-del box-6 " href="../../details.php?id=' . $wine['id'] .'" >Détails</a>
                            </div>';
             }
                echo '<div class=" index-bottle box-12 box-md-6" >
                        <h2 class="bottle-name" ><a href="./details.php?id='.$wine['id'].'" >' . htmlspecialchars(ucwords($wine['domain'])) .'</a></h2>
                        <div class="line infos-box">
                            <div class="box-12 box-sm-6 bottle-infos">
                                <h3>'. htmlspecialchars(ucfirst($wine['country'])) .'</h3>
                                <p> <span class="index-bold">Region</span> : ' . htmlspecialchars(ucwords($wine['region'])) .'  </p>
                                <p> <span class="index-bold">Grape</span> : '. htmlspecialchars(ucwords($wine['grape'])) .'</p>       
                                <p> <span class="index-bold">Year</span> : '. htmlspecialchars($wine['year']) .'</p>  
                                
                            </div>
                            <div class="box-12 box-sm-6 img-part">
                                <img class="bottle-img" src="./public/img/'. $wine['picture'] .'" alt="a wine bottle" /> 
                            </div>
                        </div>
                        '.
                        $adminBtn
                        .'
                    </div>'; 
        } 
        
        }else{
            echo '
                <h2 class="searchoff box-12">Aucune bouteille ne correspond à votre recherche ...</h2>
            ';
        }

        ?>
    </div>
    
    <div id="newsearchbox">
        <a id="newsearch-link" href="./filter_view.php" >Nouvelle recherche</a>

    </div>

    <?php

    require_once('./templates/foot.html');

    ?>